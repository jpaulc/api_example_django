from rest_framework import serializers
from .models import Language, Status

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ('id','name','paradigm')


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ('id', 'name')
