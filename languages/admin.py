from django.contrib import admin
from .models import Language, Status

# Register your models here.
admin.site.register(Language)
admin.site.register(Status)