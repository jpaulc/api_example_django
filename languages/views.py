from django.shortcuts import render
from rest_framework import viewsets
from .models import Language, Status
from .serializers import LanguageSerializer, StatusSerializer

class LanguageView(viewsets.ModelViewSet):
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer


class StatusView(viewsets.ModelViewSet):
    queryset = Status.objects.all()
    serializer_class = StatusSerializer
