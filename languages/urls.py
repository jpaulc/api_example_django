from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('languages',views.LanguageView)

status = routers.DefaultRouter()
status.register('statuses', views.StatusView)

urlpatterns = [
    path('',include(router.urls)),
    path('', include(status.urls))
]
